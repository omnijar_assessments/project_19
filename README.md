A system that takes two UK post codes and calculates the distance between the two geocoded locations.

Tested on macOS with yarn v0.24.6 and node v8.0.0
https://nodejs.org/en/download/
https://yarnpkg.com/en/docs/install

This project depends on two external API, `postcode.io` for simple 'as the crow flies' distance and `maps.googleapis.com` for car distance. 
The Google Map API asks for an authentification but for security reasons, no key is provided in this project. 
You can get one for free at https://developers.google.com/maps/web-services/.


Install all libraries

`$ yarn`

If you have a Google Map API Key

`$ GMAP_KEY="YOU_KEY" yarn start`

If you don't

`$ yarn start`
