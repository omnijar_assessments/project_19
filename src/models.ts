
// POSTCODES.IO

export type Codes = {
  admin_district: string
  admin_county: string
  admin_ward: string
  parish: string
  ccg: string
  nuts: string
}

export type Postcode = {
  postcode: string
  quality?: number
  eastings?: number
  northings?: number
  country?: string
  nhs_ha?: string
  longitude?: number
  latitude?: number
  parliamentary_constituency?: string
  european_electoral_region?: string
  primary_care_trust?: string
  region?: string
  lsoa?: string
  msoa?: string
  incode?: string
  outcode?: string
  admin_district?: string
  parish?: string
  admin_county?: string
  admin_ward?: string
  ccg?: string
  nuts?: string
  codes?: Codes
}

export type PostcodeResult = {
  query: string
  result: Postcode
}

export type Postcodes = {
  status: number
  result: PostcodeResult[]
}


// GOOGLE MAP

export type GmapUnit = 'metric' | 'imperial'

export type GmapDistance = {
  text: string
  value: number
}

export type GmapDuration = {
  text: string
  value: number
}

export type GmapElement = {
  distance: GmapDistance
  duration: GmapDuration
  status: string
}

export type GmapRow = {
  elements: GmapElement[]
}

export type GmapResult = {
  destination_addresses: string[]
  origin_addresses: string[]
  rows: GmapRow[]
  status: string
}