import { Observable } from 'rxjs/Observable'
import { Subscriber } from 'rxjs/Subscriber'
import * as http from 'http'
import * as https from 'https'

export type Verb = 'GET' | 'POST' | 'DELETE' | 'PUT'

export function ssl_query<T>(host: string, path: string, verb: Verb, headers: any, data?: any): Observable<T> {
  return Observable.create((observer: Subscriber<T>) => {
    const options: https.RequestOptions = {
      host: host,
      path: path,
      port: 443,
      method: verb,
      headers: headers
    }
    var req = https.request(options, (res: http.IncomingMessage) => {
      let body = ''
      res.on('data', chunk => body += chunk)
      res.on('end', () => {
        try {
          const json = JSON.parse(body)
          if (json == undefined || json == null) {
            console.log(`JSON from request ${verb} ${path} is undefined`)
            console.log(json)
            observer.error(`JSON from request ${verb} ${path} is undefined`)
          } else {
            observer.next(json)
            observer.complete()
          }
        } catch (e) {
          //console.log(`Parse error: ${e}`)
          observer.error(body + ' ' + e)
        }
      })
    })
    if (data != null) {
      if (verb == 'DELETE')
        console.log("deleting : " + JSON.stringify(data))
      req.write(JSON.stringify(data))
    }
    req.on('error', (e: Error) => {
      console.log(`/${path} error: ${e.message}`)
      observer.error(e.message)
    })
    req.end()
  })
}


function decimalAdjust(type, value, exp) {
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math[type](value);
  }
  value = +value;
  exp = +exp;
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
    return NaN;
  }
  if (value < 0) {
    return -decimalAdjust(type, -value, exp);
  }
  value = value.toString().split('e');
  value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

export function round10(value, exp) {
  return decimalAdjust('round', value, exp)
}