
export const jsonHeaders = {
  'Content-Type': 'application/json',
}

export const postcodeio = {
  url: 'api.postcodes.io'
}

export const gmap = {
  url: 'maps.googleapis.com'
}