import { Observable } from 'rxjs/Observable'
import { Subscriber } from 'rxjs/Subscriber'
import * as R from 'ramda'

import { Postcodes, PostcodeResult, Postcode, Codes, GmapResult, GmapUnit } from './models'
import * as config from './config'
import * as utils from './utils'

import { ReadLine, createInterface } from "readline"

const readLine = createInterface({
  input: process.stdin,
  output: process.stdout
});

function getPostcodes(postcodes: string[]): Observable<Postcodes> {
  return utils.ssl_query(config.postcodeio.url, '/postcodes', 'POST', config.jsonHeaders, { 'postcodes': postcodes })
}

function getGmapDistance(key: string, lat1: number, lon1: number, lat2: number, lon2: number, units: GmapUnit): Observable<GmapResult> {
  return utils.ssl_query(config.gmap.url, `/maps/api/distancematrix/json?units=${units}&origins=${lat1},${lon1}&destinations=${lat2},${lon2}&key=${key}`, 'GET', config.jsonHeaders)
}

function rad(deg: number): number {
  return deg * Math.PI / 180
}

function distanceAsTheCrowFlies(_lat1: number, lon1: number, _lat2: number, lon2: number): number {
  const R = 6371 //km
  const lat1 = rad(_lat1)
  const lat2 = rad(_lat2)
  const dlon = rad(lon2 - lon1)
  const dlat = rad(_lat2 - _lat1)
  const a = (Math.sin(dlat / 2)) * (Math.sin(dlat / 2)) + Math.cos(lat1) * Math.cos(lat2) * (Math.sin(dlon / 2)) * (Math.sin(dlon / 2))
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  return R * c
}

function distance(postcode1: string, postcode2: string) {
  const postCodesObs: Observable<Postcodes> = getPostcodes([postcode1, postcode2])
  postCodesObs.subscribe((res: Postcodes) => {
    if (res.status == 200 && res.result != null && res.result.length == 2 && R.find(r => r.result == null, res.result) == null) {
      const postcode1 = res.result[0].result
      const postcode2 = res.result[1].result
      if (postcode1.longitude != null && postcode1.latitude != null && postcode2.longitude != null && postcode2.latitude != null) {
        const d1 = distanceAsTheCrowFlies(postcode1.latitude, postcode1.longitude, postcode2.latitude, postcode2.longitude)
        const res = `\nDistance from ${postcode1.postcode} to ${postcode2.postcode}\n`
          + `As the crow flies : ${utils.round10(d1, -1)} km`
        console.log(res)
        const gmap_key = process.env.GMAP_KEY
        if (gmap_key != null && gmap_key != "") {
          getGmapDistance(gmap_key, postcode1.latitude, postcode1.longitude, postcode2.latitude, postcode2.longitude, 'metric')
            .subscribe((res: GmapResult) => {
              res.rows.forEach(r => r.elements.forEach(e => {
                console.log(`By car : ${e.distance.text}\n`)
              }))
              readLine.prompt()
            }, error => {
              console.error(`error getting Google Map distance: ${error}`)
              readLine.prompt()
            })
        } else {
          console.log('\n')
          readLine.prompt()
        }
      }
      else {
        console.error("postcode result has null values")
        readLine.prompt()
      }
    }
    else {
      console.error("postcode result has null values")
      readLine.prompt()
    }
  }, error => {
    console.error(`error getting postcodes: ${error}`)
    readLine.prompt()
  })
}

function main() {
  readLine.setPrompt("Please, enter two UK zipcodes separated by a coma\n(ex: NW3 5UH, EH1 1RE)\n")
  readLine.on("line", (line: string) => {
    if (line) {
      const codes = line.replace(/\s/g, '').split(',')
      if (codes.length == 2) {
        distance(codes[0], codes[1])
      } else {
        readLine.prompt()
      }
    }
    else
      readLine.emit("end")
  }).on("end", () => {
    process.exit()
  });
  readLine.prompt()
}

main()